GOCMD=go

GOBUILD=$(GOCMD) build
GORUN=$(GOCMD) run

VERSION=`cat .version | tr -d "[:space:]"`
ifndef COMMIT_SHA
override COMMIT_SHA = `git rev-parse --short HEAD`
endif
BUILDTIME=`date +%FT%T%z`

GOOPTS=-ldflags "-X cli-template/internal/version.Version=${VERSION} -X cli-template/internal/version.Build=${COMMIT_SHA} -X cli-template/internal/version.BuildTime=${BUILDTIME}"

BINARY_NAME=cli-template
BINARY_DIR=releases
BINARY_DARWIN=$(BINARY_NAME)_darwin
BINARY_UNIX=$(BINARY_NAME)_unix

.PHONY: all clean build-darwin build-linux

all: clean build-darwin build-linux

build: clean
	$(GOBUILD) -o $(BINARY_NAME) -v $(GOOPTS)

run:
	$(GORUN) $(GOOPTS) .  $(RUN_ARGS)

clean:
	rm -r $(BINARY_NAME) || true
	rm -rf $(BINARY_DIR) || true

# Cross compilation
build-darwin:
	CGO_ENABLED=0 GOOS=darwin GOARCH=amd64 $(GOBUILD) -o $(BINARY_DIR)/$(BINARY_DARWIN) -v $(GOOPTS)

build-linux:
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 $(GOBUILD) -o $(BINARY_DIR)/$(BINARY_UNIX) -v $(GOOPTS)
