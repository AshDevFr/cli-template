package logger

import (
	"github.com/sirupsen/logrus"
	"io"
)

var logger *logrus.Logger

type Fields logrus.Fields

func init() {
	logger = logrus.New()
}

func InitLogger() {
	formatter := &logrus.TextFormatter{
		FullTimestamp: true,
	}
	logger.SetFormatter(formatter)
}

func SetLogger(newLogger *logrus.Logger) {
	logger = newLogger
}

func SetDebugMode() {
	logger.SetLevel(logrus.DebugLevel)
}

func SetOutput(out io.Writer) {
	logger.SetOutput(out)
}

func Debug(args ...interface{}) {
	logger.Debug(args...)
}

func Debugf(msg string, args ...interface{}) {
	logger.Debugf(msg, args...)
}

func Info(args ...interface{}) {
	logger.Info(args...)
}

func Infof(msg string, args ...interface{}) {
	logger.Infof(msg, args...)
}

func Warn(args ...interface{}) {
	logger.Warn(args...)
}

func Warnf(msg string, args ...interface{}) {
	logger.Warnf(msg, args...)
}

func Error(args ...interface{}) {
	logger.Error(args...)
}

func Errorf(msg string, args ...interface{}) {
	logger.Errorf(msg, args...)
}

func Fatal(args ...interface{}) {
	logger.Fatal(args...)
}

func Fatalf(msg string, args ...interface{}) {
	logger.Fatalf(msg, args...)
}

func WithFields(fields Fields) *logrus.Entry {
	return logger.WithFields(logrus.Fields(fields))
}
