package version

import (
	"cli-template/internal/logger"
	"fmt"
	"github.com/sirupsen/logrus"
	logrustest "github.com/sirupsen/logrus/hooks/test"
	"github.com/stretchr/testify/require"
	"testing"
)

func TestDebug(t *testing.T) {
	Version = "1.0.0"
	Build = "abcdef12345"
	BuildTime = "2022-01-20T11: 25:22-0700"

	t.Run("Debug the version", func(t *testing.T) {
		testLogger, hook := logrustest.NewNullLogger()
		testLogger.ExitFunc = func(code int) {}
		logger.SetLogger(testLogger)
		logger.SetDebugMode()

		Debug()

		require.Len(t, hook.Entries, 1)
		require.Equal(t, logrus.DebugLevel, hook.LastEntry().Level)
		require.Equal(t, "Version", hook.LastEntry().Message)
		require.Equal(t, logrus.Fields{
			"Version":    Version,
			"Build":      Build,
			"Build Time": BuildTime,
		}, hook.LastEntry().Data)
	})
}

func TestString(t *testing.T) {
	Version = "1.0.0"
	Build = "abedef12345"
	BuildTime = "2022-01-20T11:25:22-0700"

	t.Run("Return the version", func(t *testing.T) {
		output := String()

		require.Contains(t, output, fmt.Sprintf("Version: %s", Version))
		require.Contains(t, output, fmt.Sprintf("Build: %s", Build))
		require.Contains(t, output, fmt.Sprintf("Build Time: %s", BuildTime))
	})
}
