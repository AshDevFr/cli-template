package version

import (
	"cli-template/internal/logger"
	"fmt"
)

var (
	Version   string
	Build     string
	BuildTime string
)

func Debug() {
	logger.WithFields(logger.Fields{
		"Version":    Version,
		"Build":      Build,
		"Build Time": BuildTime,
	}).Debug("Version")
}

func String() string {
	return fmt.Sprintf("Version: %s\nBuild: %s\nBuild Time: %s", Version, Build, BuildTime)
}
