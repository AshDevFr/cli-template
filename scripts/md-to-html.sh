#!/bin/sh

SCRIPT_DIR=$(dirname "$0")
DOC_PATH=${1:-"."}

cp "$DOC_PATH/cli-template.md" "$DOC_PATH/index.md"

find "$DOC_PATH" -type f -iname "*.md" | while IFS= read -r file; do
  filedir=$(dirname "$file")
  filename=$(basename "${file%.*}")
  pandoc "$filedir/$filename.md" \
    -f gfm \
    --toc \
    -t html \
    --lua-filter=$SCRIPT_DIR/links-to-html.lua \
    -s \
    -o "$filedir/$filename.html"
done