package main

import (
	"cli-template/cmd"
	"cli-template/internal/logger"
)

func main() {
	logger.InitLogger()

	cmd.Execute()
}
