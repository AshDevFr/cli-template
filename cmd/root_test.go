package cmd

import (
	"cli-template/internal/logger"
	"cli-template/test"
	"github.com/sirupsen/logrus"
	logrustest "github.com/sirupsen/logrus/hooks/test"
	"github.com/stretchr/testify/require"
	"testing"
)

func TestRootPreRun(t *testing.T) {
	t.Run("Don't print out the version when debug is not enabled", func(t *testing.T) {
		testLogger, hook := logrustest.NewNullLogger()
		testLogger.ExitFunc = func(code int) {}
		logger.SetLogger(testLogger)

		output, err := test.ExecuteCommand(rootCmd, "help")
		require.Nil(t, err)

		require.Len(t, hook.Entries, 0)
		require.Contains(t, output, "Usage:\n  cli-template [command]\n")
	})

	t.Run("Print out the version when debug is enabled", func(t *testing.T) {
		testLogger, hook := logrustest.NewNullLogger()
		testLogger.ExitFunc = func(code int) {}
		logger.SetLogger(testLogger)

		output, err := test.ExecuteCommand(rootCmd, "-v", "help")
		require.Nil(t, err)

		require.Len(t, hook.Entries, 2)
		require.Equal(t, logrus.DebugLevel, hook.Entries[0].Level)
		require.Equal(t, "Verbose Mode", hook.Entries[0].Message)

		require.Equal(t, logrus.DebugLevel, hook.Entries[1].Level)
		require.Equal(t, "Version", hook.Entries[1].Message)

		require.Contains(t, output, "Usage:\n  cli-template [command]\n")
	})
}
