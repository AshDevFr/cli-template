package cmd

import (
	"cli-template/internal/version"
	"github.com/spf13/cobra"
)

var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "Print the version information",
	Run: func(cmd *cobra.Command, args []string) {
		cmd.Println(version.String())
	},
}

func init() {
	rootCmd.AddCommand(versionCmd)
}
