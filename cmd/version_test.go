package cmd

import (
	"cli-template/test"
	"github.com/spf13/cobra"
	"github.com/stretchr/testify/require"
	"testing"
)

func TestVersionCmd(t *testing.T) {
	t.Run("Don't print out the version when debug is not enabled", func(t *testing.T) {
		rootCmd := &cobra.Command{}
		rootCmd.AddCommand(versionCmd)

		output, err := test.ExecuteCommand(rootCmd, "version")
		require.Nil(t, err)

		require.Contains(t, output, "Version: ")
		require.Contains(t, output, "Build: ")
		require.Contains(t, output, "Build Time: ")
	})
}
