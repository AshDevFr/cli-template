package cmd

import (
	"cli-template/test"
	"fmt"
	"github.com/spf13/cobra"
	"github.com/stretchr/testify/require"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
	"testing"
)

var originalDocDir = docDir

func resetDocDir() {
	docDir = originalDocDir
}

func TestDocsCmd(t *testing.T) {
	tmpdir := t.TempDir()

	t.Run("Generate the docs in the default dir", func(t *testing.T) {
		t.Cleanup(resetDocDir)

		rootCmd := &cobra.Command{Use: "root"}
		rootCmd.AddCommand(docsCmd)

		docDir = path.Join(tmpdir, "docs123")

		_, err := os.Stat(docDir)
		require.NotNil(t, err)

		_, err = test.ExecuteCommand(rootCmd, "docs")
		require.Nil(t, err)

		_, err = os.Stat(docDir)
		require.Nil(t, err)

		docFile := filepath.Join(docDir, "root_docs.md")
		_, err = os.Stat(docFile)
		require.Nil(t, err)

		content, err := ioutil.ReadFile(docFile)
		require.Nil(t, err, fmt.Sprintf("failed to open the doc file %s: %v", docFile, err))

		require.Contains(t, string(content), "root docs [flags]")
	})

	t.Run("Generate the docs in the default dir whe it exists", func(t *testing.T) {
		t.Cleanup(resetDocDir)

		rootCmd := &cobra.Command{Use: "root"}
		rootCmd.AddCommand(docsCmd)

		docDir = path.Join(tmpdir, "docs234")

		err := os.MkdirAll(docDir, os.ModePerm)
		require.Nil(t, err)

		_, err = os.Stat(docDir)
		require.Nil(t, err)

		_, err = test.ExecuteCommand(rootCmd, "docs")
		require.Nil(t, err)

		_, err = os.Stat(docDir)
		require.Nil(t, err)

		docFile := filepath.Join(docDir, "root_docs.md")
		_, err = os.Stat(docFile)
		require.Nil(t, err)

		content, err := ioutil.ReadFile(docFile)
		require.Nil(t, err, fmt.Sprintf("failed to open the doc file %s: %v", docFile, err))

		require.Contains(t, string(content), "root docs [flags]")
	})

	t.Run("Generate the docs in the specified dir", func(t *testing.T) {
		t.Cleanup(resetDocDir)

		rootCmd := &cobra.Command{Use: "root"}
		rootCmd.AddCommand(docsCmd)

		docPath := path.Join(tmpdir, "docs345")

		_, err := os.Stat(docPath)
		require.NotNil(t, err)

		_, err = test.ExecuteCommand(rootCmd, "docs", "-o", docPath)
		require.Nil(t, err)

		_, err = os.Stat(docPath)
		require.Nil(t, err)

		docFile := filepath.Join(docPath, "root_docs.md")
		_, err = os.Stat(docFile)
		require.Nil(t, err)

		content, err := ioutil.ReadFile(docFile)
		require.Nil(t, err, fmt.Sprintf("failed to open the doc file %s: %v", docFile, err))

		require.Contains(t, string(content), "root docs [flags]")
	})
}
