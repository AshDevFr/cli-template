package cmd

import (
	"cli-template/internal/logger"
	"github.com/spf13/cobra"
	"github.com/spf13/cobra/doc"
	"os"
	"path/filepath"
)

var docsCmd = &cobra.Command{
	Use:   "docs",
	Short: "Generate the documentation for the CLI",
	Args:  cobra.NoArgs,
	Run: func(cmd *cobra.Command, args []string) {
		if _, err := os.Stat(docDir); os.IsNotExist(err) {
			err := os.MkdirAll(docDir, os.ModePerm)
			if err != nil {
				logger.Fatal(err)
			}
		}

		err := doc.GenMarkdownTree(rootCmd, docDir)
		if err != nil {
			logger.Fatal(err)
		}

		logger.Infof("The documentation was generated in '%s'", docDir)
	},
}

var (
	docDir string
)

func init() {
	docsCmd.Flags().StringVarP(&docDir, "output", "o", filepath.Join(".", "docs"), "Output directory (default docs)")

	rootCmd.AddCommand(docsCmd)
}
